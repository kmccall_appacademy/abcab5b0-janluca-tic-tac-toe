class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
    @mark = mark
  end

  def get_move
    puts "where"
    num = gets.chomp.split("")
    [num[0].to_i, num[3].to_i]
  end

  def display(board)
    board.grid.each do |row|
      puts "#{row}"
    end
  end

end
