class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    moves_left.each do |move|
      if wins?(move) == true
        return move
      end
    end
    moves_left.sample
  end

  def moves_left
    moves = []
    outer = 0
    3.times do
      inner = 0
      3.times do
        board.grid[outer][inner] == nil ? moves << [outer, inner] : nil
        inner += 1
      end
      outer += 1
    end
    moves
  end

  def wins?(move)
    board.grid[move[0]][move[1]] = @mark
    if board.winner == @mark
      board.grid[move[0]][move[1]] = nil
      true
    else
      board.grid[move[0]][move[1]] = nil
      false
    end
  end

end
