N = nil

class Board
  attr_accessor :grid

  def initialize(grid = [[N, N, N], [N, N, N], [N, N, N]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    return true if @grid[pos[0]][pos[1]] == nil
    false
  end

  def rows
    @grid
  end

  def columns
    columns = []
    i = 0
    grid[0].count.times do
      column = []
      j = 0
      grid.count.times do
        column << grid[j][i]
        j += 1
      end
      i += 1
      columns << column
    end
    columns
  end

  def up_diagonal
    diagonal = []
    i = 0
    grid[0].count.times do
      diagonal << grid[i][i]
      i += 1
    end
    diagonal
  end

  def down_diagonal
    diagonal = []
    i = grid[0].count - 1
    j = 0
    grid[0].count.times do
      diagonal << grid[i][j]
      i -= 1
      j += 1
    end
    diagonal
  end

  def winner
    combinations = rows + columns << up_diagonal << down_diagonal
    combinations.each do |group|
      return :X if group == [:X, :X, :X]
      return :O if group == [:O, :O, :O]
    end
    nil
  end

  def over?
    return true if self.winner
    any_nil = false
    @grid.each do |container|
      if container.any? { |mark| mark == nil }
        any_nil = true
      end
    end
    any_nil ? false : true
  end

end
