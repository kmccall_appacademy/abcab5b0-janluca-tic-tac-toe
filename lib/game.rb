require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :current_player

  def initialize(p1 = HumanPlayer.new(player_one), p2 = ComputerPlayer.new(player_two))
    @player_one = p1
    @player_two = p2
    @board = Board.new
    @current_player = @player_one
  end

  def current_player
    @current_player
  end

  def play_turn
    @current_player.display
    move = @current_player.get_move
    @board.place_mark(move, @current_player.mark)
    switch_players!
  end



  def switch_players!
    @current_player = (@current_player == @player_one) ? @player_two : @player_one
  end




end
